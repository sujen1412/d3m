d3m\.metadata package
=====================

Submodules
----------

d3m\.metadata\.base module
--------------------------

.. automodule:: d3m.metadata.base
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.metadata\.hyperparams module
---------------------------------

.. automodule:: d3m.metadata.hyperparams
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.metadata\.params module
----------------------------

.. automodule:: d3m.metadata.params
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.metadata\.pipeline module
------------------------------

.. automodule:: d3m.metadata.pipeline
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.metadata\.problem module
-----------------------------

.. automodule:: d3m.metadata.problem
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: d3m.metadata
    :members:
    :undoc-members:
    :show-inheritance:
