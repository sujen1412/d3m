d3m\.primitive\_interfaces package
==================================

Submodules
----------

d3m\.primitive\_interfaces\.base module
---------------------------------------

.. automodule:: d3m.primitive_interfaces.base
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.clustering module
---------------------------------------------

.. automodule:: d3m.primitive_interfaces.clustering
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.distance module
-------------------------------------------

.. automodule:: d3m.primitive_interfaces.distance
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.featurization module
------------------------------------------------

.. automodule:: d3m.primitive_interfaces.featurization
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.generator module
--------------------------------------------

.. automodule:: d3m.primitive_interfaces.generator
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.supervised\_learning module
-------------------------------------------------------

.. automodule:: d3m.primitive_interfaces.supervised_learning
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.transformer module
----------------------------------------------

.. automodule:: d3m.primitive_interfaces.transformer
    :members:
    :undoc-members:
    :show-inheritance:

d3m\.primitive\_interfaces\.unsupervised\_learning module
---------------------------------------------------------

.. automodule:: d3m.primitive_interfaces.unsupervised_learning
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: d3m.primitive_interfaces
    :members:
    :undoc-members:
    :show-inheritance:
