import os
import pickle
import sys
import typing
import unittest

from d3m import container, index, runtime, utils
from d3m.metadata import pipeline
from d3m.primitive_interfaces import base

TEST_PRIMITIVES_DIR = os.path.join(os.path.dirname(__file__), 'data', 'primitives')

sys.path.insert(0, TEST_PRIMITIVES_DIR)

from test_primitives.monomial import MonomialPrimitive
from test_primitives.random import RandomPrimitive
from test_primitives.sum import SumPrimitive
from test_primitives.increment import IncrementPrimitive


class Resolver(pipeline.Resolver):
    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:
        # To hide any logging or stdout output.
        with utils.silence():
            return super()._get_primitive(primitive_description)


class TestRuntime(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # To hide any logging or stdout output.
        with utils.silence():
            index.register_primitive('d3m.primitives.test.MonomialPrimitive', MonomialPrimitive)
            index.register_primitive('d3m.primitives.test.RandomPrimitive', RandomPrimitive)
            index.register_primitive('d3m.primitives.test.SumPrimitive', SumPrimitive)
            index.register_primitive('d3m.primitives.test.IncrementPrimitive', IncrementPrimitive)

    def test_basic(self):
        with open(os.path.join(os.path.dirname(__file__), 'data', 'pipelines', 'random-sample.yml'), 'r') as pipeline_file:
            p = pipeline.Pipeline.from_yaml(pipeline_file, resolver=Resolver())

        r = runtime.Runtime(p)

        inputs = [container.List([0, 1, 42])]

        outputs = r.fit(inputs)

        self.assertEqual(len(outputs), 1)

        dataframe = outputs[0]

        self.assertEqual(dataframe.values.tolist(), [
            [1.764052345967664 + 1],
            [0.4001572083672233 + 1],
            [-1.7062701906250126 + 1],
        ])

        outputs = r.produce(inputs)

        self.assertEqual(len(outputs), 1)

        dataframe = outputs[0]

        self.assertEqual(dataframe.values.tolist(), [
            [1.764052345967664 + 1],
            [0.4001572083672233 + 1],
            [-1.7062701906250126 + 1],
        ])

        pickled = pickle.dumps(r)
        restored = pickle.loads(pickled)

        outputs = restored.produce(inputs)

        self.assertEqual(len(outputs), 1)

        dataframe = outputs[0]

        self.assertEqual(dataframe.values.tolist(), [
            [1.764052345967664 + 1],
            [0.4001572083672233 + 1],
            [-1.7062701906250126 + 1],
        ])

        r = runtime.Runtime(p, random_seed=42)

        inputs = [container.List([0, 1, 42])]

        outputs = r.fit(inputs)

        self.assertEqual(len(outputs), 1)

        dataframe = outputs[0]

        self.assertEqual(dataframe.values.tolist(), [
            [0.4967141530112327 + 1],
            [-0.13826430117118466 + 1],
            [-0.11564828238824053 + 1],
        ])

        r = runtime.Runtime(p, [{}, {'amount': 10}], random_seed=42)

        inputs = [container.List([0, 1, 42])]

        outputs = r.fit(inputs)

        self.assertEqual(len(outputs), 1)

        dataframe = outputs[0]

        self.assertEqual(dataframe.values.tolist(), [
            [0.4967141530112327  + 10],
            [-0.13826430117118466 + 10],
            [-0.11564828238824053 + 10],
        ])


if __name__ == '__main__':
    unittest.main()
